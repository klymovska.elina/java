package spd.trello;

import com.jayway.jsonpath.JsonPath;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.test.web.servlet.MvcResult;
import spd.trello.domain.User;


import java.time.LocalDateTime;
import java.util.UUID;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertAll;

@SpringBootTest
public class UserIntegrationTest extends AbstractIntegrationTest<User> {
    private final String URL = "/users";

    @Test
    public void successCreate() throws Exception {
        User user = new User();
        user.setFirstName("name");
        user.setLastName("surname");
        user.setEmail("a@gmail.com");
        user.setCreatedBy("a@gmail.com");
        user.setUpdatedBy("b@gmail.com");
        LocalDateTime localDateTime = LocalDateTime.now();
        MvcResult result = super.create(URL, user);
        LocalDateTime createDate = LocalDateTime.parse(getValue(result, "$.createdDate").toString());
        LocalDateTime updateDate = LocalDateTime.parse(getValue(result, "$.updatedDate").toString());
        assertAll(
                () -> assertNotNull(getValue(result, "$.id")),
                () -> assertEquals(HttpStatus.CREATED.value(), result.getResponse().getStatus()),
                () -> assertEquals(user.getFirstName(), getValue(result, "$.firstName")),
                () -> assertEquals(user.getLastName(), getValue(result, "$.lastName")),
                () -> assertEquals(user.getEmail(), getValue(result, "$.email")),
                () -> assertEquals(user.getCreatedBy(), getValue(result, "$.createdBy")),
                () -> assertEquals(user.getUpdatedBy(), getValue(result, "$.updatedBy")),
                () -> assertEquals(localDateTime.minusNanos(localDateTime.getNano()), createDate.minusNanos(createDate.getNano())),
                () -> assertEquals(localDateTime.minusNanos(localDateTime.getNano()), updateDate.minusNanos(updateDate.getNano())));
    }

    @Test
    public void createFailure() throws Exception {
        User entity = new User();
        entity.setLastName("surname");
        entity.setEmail("wedfgh");
        MvcResult mvcResult = super.create(URL, entity);
        Assertions.assertEquals(HttpStatus.BAD_REQUEST.value(), mvcResult.getResponse().getStatus());
    }


    @Test
    public void updateSuccess() throws Exception {
        User user = new User();
        user.setFirstName("name");
        user.setLastName("surname");
        user.setEmail("a@gmail.com");
        user.setCreatedBy("a@gmail.com");
        user.setUpdatedBy("b@gmail.com");
        super.create(URL, user);

        User updateUser = new User();
        updateUser.setFirstName("name2");
        updateUser.setLastName("surname2");
        updateUser.setEmail("a2@gmail.com");
        updateUser.setCreatedBy("a@gmail.com");
        updateUser.setUpdatedBy("b2@gmail.com");
        updateUser.setUpdatedDate(LocalDateTime.now());

        LocalDateTime localDateTime = LocalDateTime.now();

        MvcResult result = super.update(URL, user.getId(), updateUser);

        LocalDateTime createDate = LocalDateTime.parse(getValue(result, "$.createdDate").toString());
        LocalDateTime updateDate = LocalDateTime.parse(getValue(result, "$.updatedDate").toString());
        assertAll(
                () -> assertNotNull(getValue(result, "$.id")),
                () -> assertEquals(HttpStatus.OK.value(), result.getResponse().getStatus()),
                () -> assertEquals(updateUser.getFirstName(), getValue(result, "$.firstName")),
                () -> assertEquals(updateUser.getLastName(), getValue(result, "$.lastName")),
                () -> assertEquals(updateUser.getEmail(), getValue(result, "$.email")),
                () -> assertEquals(updateUser.getCreatedBy(), getValue(result, "$.createdBy")),
                () -> assertEquals(updateUser.getUpdatedBy(), getValue(result, "$.updatedBy")),
                () -> assertEquals(localDateTime.minusNanos(localDateTime.getNano()), createDate.minusNanos(createDate.getNano())),
                () -> assertEquals(localDateTime.minusNanos(localDateTime.getNano()), updateDate.minusNanos(updateDate.getNano())));
    }

    @Test
    void updateFailed() throws Exception {
        User user = new User();
        user.setFirstName("name");
        user.setLastName("surname");
        user.setEmail("email@gmail.com");
        user.setCreatedBy("a@gmail.com");
        user.setUpdatedBy("b2@gmail.com");
        super.create(URL, user);


        User updateUser = new User();
        updateUser.setLastName("surname2");
        updateUser.setEmail("a2@gmail.com");
        updateUser.setCreatedBy("a@gmail.com");
        updateUser.setUpdatedBy("b2@gmail.com");
        MvcResult result = super.update(URL, user.getId(), updateUser);

        assertEquals(HttpStatus.BAD_REQUEST.value(), result.getResponse().getStatus());
    }

    @Test
    void deleteSuccess() throws Exception {
        User entity = new User();
        entity.setFirstName("name");
        entity.setLastName("surname");
        entity.setEmail("a@gmail.com");
        entity.setCreatedBy("a@gmail.com");
        entity.setUpdatedBy("b@gmail.com");
        entity.setUpdatedDate(LocalDateTime.now());
        entity.setCreatedDate(LocalDateTime.now());

        MvcResult ent = super.create(URL, entity);
        UUID id = UUID.fromString(JsonPath.read(ent.getResponse().getContentAsString(), "$.id"));

        MvcResult result = super.delete(URL, id);
        assertAll(
                () -> Assertions.assertEquals(HttpStatus.OK.value(), result.getResponse().getStatus()),
                () -> Assertions.assertEquals(HttpStatus.NOT_FOUND.value(), super.readById(URL, id).getResponse().getStatus())
        );
    }

    @Test
    void deleteFailed() throws Exception {
        MvcResult result = super.delete("/wrong", UUID.randomUUID());

        assertEquals(HttpStatus.NOT_FOUND.value(), result.getResponse().getStatus());
    }


    @Test
    void getByIdSuccess() throws Exception {
        User entity = new User();
        entity.setFirstName("name");
        entity.setLastName("surname");
        entity.setEmail("a@gmail.com");
        entity.setCreatedBy("a@gmail.com");
        entity.setUpdatedBy("b@gmail.com");
        entity.setUpdatedDate(LocalDateTime.now());
        entity.setCreatedDate(LocalDateTime.now());
        MvcResult ent = super.create(URL, entity);
        UUID id = UUID.fromString(JsonPath.read(ent.getResponse().getContentAsString(), "$.id"));
        MvcResult result = super.readById(URL, id);
        Assertions.assertNotNull(getValue(result, "$.id"));
    }

    @Test
    void getByIdFailure() throws Exception {
        UUID id = UUID.randomUUID();
        MvcResult result = super.readById(URL, id);
        Assertions.assertEquals(HttpStatus.NOT_FOUND.value(), result.getResponse().getStatus());
    }

    @Test
    void getAllSuccess() throws Exception {
        MvcResult result = super.readAll(URL);
        assertAll(
                () -> Assertions.assertEquals(HttpStatus.OK.value(), result.getResponse().getStatus()),
                () -> Assertions.assertEquals("application/json", result.getResponse().getContentType())
        );
    }

    @Test
    void getAllFailure() throws Exception {
        MvcResult result = super.readAll("/null");
        Assertions.assertEquals(HttpStatus.NOT_FOUND.value(), result.getResponse().getStatus());
    }
}


