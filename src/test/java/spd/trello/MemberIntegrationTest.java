package spd.trello;

import com.jayway.jsonpath.JsonPath;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import spd.trello.domain.Member;
import spd.trello.domain.User;
import spd.trello.domain.type.Role;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;


import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
public class MemberIntegrationTest extends AbstractIntegrationTest<Member> {

    private final String URL = "/members";
    @Autowired
    private Creator creator;

    @Test
    public void successCreate() throws Exception {
        User user = creator.getNewUser("newUser");
        Member firstMember = new Member();
        firstMember.setRole(Role.GUEST);
        firstMember.setUser(user);
        firstMember.setCreatedBy(user.getEmail());
        LocalDateTime localDateTime1 = LocalDateTime.now();
        MvcResult firstMvcResult = super.create(URL, firstMember);

        LocalDateTime createDate = LocalDateTime.parse(getValue(firstMvcResult, "$.createdDate").toString());
        LocalDateTime updateDate = LocalDateTime.parse(getValue(firstMvcResult, "$.updatedDate").toString());

        Member secondMember = new Member();
        secondMember.setRole(Role.MEMBER);
        secondMember.setUser(user);
        secondMember.setCreatedBy(user.getEmail());

        LocalDateTime localDateTime2 = LocalDateTime.now();
        MvcResult secondMvcResult = super.create(URL, secondMember);
        LocalDateTime createDate2 = LocalDateTime.parse(getValue(secondMvcResult, "$.createdDate").toString());
        LocalDateTime updateDate2 = LocalDateTime.parse(getValue(secondMvcResult, "$.updatedDate").toString());

        assertAll(
                () -> assertEquals(HttpStatus.CREATED.value(), firstMvcResult.getResponse().getStatus()),
                () -> assertNotNull(getValue(firstMvcResult, "$.id")),
                () -> assertTrue(getValue(firstMvcResult, "$.user").toString().contains(user.getId().toString())),
                () -> assertEquals(firstMember.getCreatedBy(), getValue(firstMvcResult, "$.createdBy")),
                () -> assertEquals(firstMember.getUpdatedBy(), getValue(firstMvcResult, "$.updatedBy")),
                () -> assertEquals(localDateTime1.minusNanos(localDateTime1.getNano()), createDate.minusNanos(createDate.getNano())),
                () -> assertEquals(localDateTime1.minusNanos(localDateTime1.getNano()), updateDate.minusNanos(updateDate.getNano())),

                () -> assertEquals(HttpStatus.CREATED.value(), secondMvcResult.getResponse().getStatus()),
                () -> assertNotNull(getValue(secondMvcResult, "$.id")),
                () -> assertTrue(getValue(firstMvcResult, "$.user").toString().contains(user.getId().toString())),
                () -> assertEquals(secondMember.getCreatedBy(), getValue(secondMvcResult, "$.createdBy")),
                () -> assertEquals(secondMember.getUpdatedBy(), getValue(secondMvcResult, "$.updatedBy")),
                () -> assertEquals(localDateTime2.minusNanos(localDateTime2.getNano()), createDate2.minusNanos(createDate2.getNano())),
                () -> assertEquals(localDateTime2.minusNanos(localDateTime2.getNano()), updateDate2.minusNanos(updateDate2.getNano())));
    }

    @Test
    public void createFailure() throws Exception {
        Member entity = new Member();
        MvcResult mvcResult = super.create(URL, entity);
        assertEquals(HttpStatus.BAD_REQUEST.value(), mvcResult.getResponse().getStatus());
    }

    @Test
    public void update() throws Exception {
        Member member = creator.getNewMember("a@gmail.com");

        User user = creator.getNewUser("updating@gmail.com");
        Member updateMember = new Member();
        updateMember.setRole(Role.GUEST);
        user.setCreatedDate(LocalDateTime.now());
        updateMember.setCreatedBy(user.getEmail());
        updateMember.setUser(user);
        updateMember.setUpdatedBy(member.getCreatedBy());
        updateMember.setRole(Role.ADMIN);
        LocalDateTime localDateTime = LocalDateTime.now();
        MvcResult mvcResult = super.update(URL, member.getId(), member);
        LocalDateTime createDate = LocalDateTime.parse(getValue(mvcResult, "$.createdDate").toString());
        LocalDateTime updateDate = LocalDateTime.parse(getValue(mvcResult, "$.updatedDate").toString());
        assertAll(
                () -> assertEquals(HttpStatus.OK.value(), mvcResult.getResponse().getStatus()),
                () -> assertNotNull(getValue(mvcResult, "$.id")),
                () -> assertTrue(getValue(mvcResult, "$.user").toString().contains(member.getUser().getId().toString())),
                () -> assertEquals(member.getRole().toString(), getValue(mvcResult, "$.role")),
                () -> assertEquals(member.getCreatedBy(), getValue(mvcResult, "$.createdBy")),
                () -> assertEquals(member.getUpdatedBy(), getValue(mvcResult, "$.updatedBy")),
                () -> assertEquals(localDateTime.minusNanos(localDateTime.getNano()), createDate.minusNanos(createDate.getNano())),
                () -> assertEquals(localDateTime.minusNanos(localDateTime.getNano()), updateDate.minusNanos(updateDate.getNano())));
    }

    @Test
    public void updateFailure() throws Exception {
        Member firstMember = creator.getNewMember("b@gmail.com");
        firstMember.setUpdatedBy(firstMember.getCreatedBy());
        firstMember.setId(UUID.randomUUID());
        MvcResult firstMvcResult = super.update(URL, firstMember.getId(), firstMember);
        assertAll(() -> assertEquals(HttpStatus.NOT_FOUND.value(),
                firstMvcResult.getResponse().getStatus()));
    }

    @Test
    void deleteSuccess() throws Exception {
        User user = creator.getNewUser("newUser");
        Member firstMember = new Member();
        firstMember.setRole(Role.GUEST);
        firstMember.setUser(user);
        firstMember.setCreatedBy(user.getEmail());
        MvcResult firstMvcResult = super.create(URL, firstMember);
        UUID id = UUID.fromString(JsonPath.read(firstMvcResult.getResponse().getContentAsString(), "$.id"));

        MvcResult result = super.delete(URL, id);
        assertAll(
                () -> Assertions.assertEquals(HttpStatus.OK.value(), result.getResponse().getStatus()),
                () -> Assertions.assertEquals(HttpStatus.NOT_FOUND.value(), super.readById(URL, id).getResponse().getStatus())
        );
    }

    @Test
    public void deleteFailure() throws Exception {
        MvcResult result = super.delete("/wrong", UUID.randomUUID());
        assertAll(() -> assertEquals(HttpStatus.NOT_FOUND.value(),
                result.getResponse().getStatus()));
    }

    @Test
    public void findAll() throws Exception {
        Member firstMember = creator.getNewMember("d.com");
        Member secondMember = creator.getNewMember("q@gmail.com");
        MvcResult mvcResult = super.readAll(URL);
        List<Member> testMembers =getResourceArray(mvcResult);

        assertAll(
                () -> assertEquals(MediaType.APPLICATION_JSON.toString(), mvcResult.getResponse().getContentType()),
                () -> assertEquals(HttpStatus.OK.value(), mvcResult.getResponse().getStatus()),
                () -> assertTrue(testMembers.contains(firstMember)),
                () -> assertTrue(testMembers.contains(secondMember)));
    }

    @Test
    public void findById() throws Exception {
        Member member = creator.getNewMember("w@gmail.com");
        LocalDateTime localDateTime = LocalDateTime.now();
        MvcResult mvcResult = super.readById(URL, member.getId());
        LocalDateTime createDate = LocalDateTime.parse(getValue(mvcResult, "$.createdDate").toString());
        LocalDateTime updateDate = LocalDateTime.parse(getValue(mvcResult, "$.updatedDate").toString());

        assertAll(() -> assertEquals(HttpStatus.OK.value(), mvcResult.getResponse().getStatus()),
                () -> assertNotNull(getValue(mvcResult, "$.id")),
                () -> assertTrue(getValue(mvcResult, "$.user").toString().contains(member.getUser().getId().toString())),
                () -> assertEquals(member.getRole().toString(), getValue(mvcResult, "$.role")),
                () -> assertEquals(member.getCreatedBy(), getValue(mvcResult, "$.createdBy")),
                () -> assertEquals(member.getUpdatedBy(), getValue(mvcResult, "$.updatedBy")),
                () -> assertEquals(localDateTime.minusNanos(localDateTime.getNano()), createDate.minusNanos(createDate.getNano())),
                () -> assertEquals(localDateTime.minusNanos(localDateTime.getNano()), updateDate.minusNanos(updateDate.getNano())))
        ;
    }

    @Test
    public void findByIdFailure() throws Exception {
        MvcResult mvcResult = super.readById(URL, UUID.randomUUID());
        assertEquals(HttpStatus.NOT_FOUND.value(), mvcResult.getResponse().getStatus());
    }
}
