package spd.trello;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.test.web.servlet.MvcResult;
import spd.trello.domain.*;
import spd.trello.domain.common.Resource;
import spd.trello.domain.type.Role;
import spd.trello.repository.*;

import java.io.UnsupportedEncodingException;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

@Component
public class Creator {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private MemberRepository memberRepository;
    @Autowired
    private WorkspaceRepository workspaceRepository;


    public User getNewUser(String email) {
        User user = new User();
        user.setFirstName("name");
        user.setLastName("surname");
        user.setEmail(email);
        user.setCreatedBy(email);
        user.setUpdatedBy("b@gmail.com");
        user.setUpdatedDate(LocalDateTime.now());
        user.setCreatedDate(LocalDateTime.now());
        return userRepository.save(user);
    }

    public Member getNewMember(String email) {
        User user = getNewUser(email);
        Member member = new Member();
        member.setRole(Role.GUEST);
        member.setUser(user);
        member.setCreatedBy(user.getEmail());
        member.setUpdatedBy(user.getEmail());
        member.setUpdatedDate(user.getCreatedDate());
        member.setUpdatedDate(user.getUpdatedDate());
        return memberRepository.save(member);
    }


    public Workspace getNewWorkspace(String email) {
        Member member = getNewMember(email);
        Workspace workspace = new Workspace();
        workspace.setCreatedBy(member.getCreatedBy());
        workspace.setCreatedDate(LocalDateTime.now());
        workspace.setName("Space for everyone");
        Set<UUID> members = new HashSet<>();
        members.add(member.getId());
        workspace.setMembers(members);
        return workspaceRepository.save(workspace);
    }

    public Set<UUID> getIdsFromJson(String json) throws JsonProcessingException {
        return new ObjectMapper().readValue(json, new TypeReference<>() {
        });
    }

}
