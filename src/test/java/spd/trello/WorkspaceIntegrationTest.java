package spd.trello;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import spd.trello.domain.Member;
import spd.trello.domain.Workspace;
import spd.trello.domain.type.WorkspaceVisibility;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
public class WorkspaceIntegrationTest extends AbstractIntegrationTest<Workspace> {
    private final String URL = "/workspaces";

    @Autowired
    private Creator creator;

    @Test
    public void create() throws Exception {
        Member member = creator.getNewMember("r@gmail.com");
        Workspace workspace = new Workspace();
        workspace.setCreatedBy(member.getCreatedBy());
        Set<UUID> members = new HashSet<>();
        members.add(member.getId());
        workspace.setMembers(members);
        workspace.setName("Space for everyone");
        MvcResult result = super.create(URL, workspace);
        Set<UUID> testMembers = creator.getIdsFromJson(getValue(result, "$.members").toString());


        assertAll(
                () -> assertEquals(HttpStatus.CREATED.value(), result.getResponse().getStatus()),
                () -> assertNotNull(getValue(result, "$.id")),
                () -> assertEquals(workspace.getName(), getValue(result, "$.name")),
                () -> assertEquals(workspace.getDescription(), getValue(result, "$.description")),
                () -> assertEquals(WorkspaceVisibility.PUBLIC.toString(), getValue(result, "$.visibility")),
                () -> assertEquals(workspace.getCreatedBy(), getValue(result, "$.createdBy")),
                () -> assertEquals(String.valueOf(LocalDate.now()), getValue(result, "$.createdDate")),
                () -> assertNull(getValue(result, "$.updatedBy")),
                () -> assertNull(getValue(result, "$.updatedDate")),
                () -> assertEquals(1, testMembers.size())
        );
    }

    @Test
    public void createFailure() throws Exception {
        MvcResult mvcResult = super.create(URL, new Workspace());
        assertEquals(HttpStatus.BAD_REQUEST.value(), mvcResult.getResponse().getStatus());
    }

    @Test
    public void update() throws Exception {
        Workspace workspace = creator.getNewWorkspace("н@gmail.com");
        Member member = creator.getNewMember("г@gmail.com");
        workspace.setUpdatedBy(workspace.getCreatedBy());
        workspace.setUpdatedDate(LocalDateTime.now());
        workspace.setName("Workspace2");
        workspace.setDescription("description");
        workspace.setVisibility(WorkspaceVisibility.PUBLIC);
        Set<UUID> members = workspace.getMembers();
        members.add(member.getId());
        workspace.setMembers(members);
        MvcResult mvcResult = super.update(URL, workspace.getId(), workspace);
        Set<UUID> test = creator.getIdsFromJson(getValue(mvcResult, "$.members").toString());
        assertAll(
                () -> assertEquals(HttpStatus.OK.value(), mvcResult.getResponse().getStatus()),
                () -> assertNotNull(getValue(mvcResult, "$.id")),
                () -> assertEquals(workspace.getName(), getValue(mvcResult, "$.name")),
                () -> assertEquals(workspace.getDescription(), getValue(mvcResult, "$.description")),
                () -> assertEquals(WorkspaceVisibility.PUBLIC.toString(), getValue(mvcResult, "$.visibility")),
                () -> assertTrue(test.contains(workspace.getMembers().iterator().next())),
                () -> assertTrue(test.contains(workspace.getMembers().iterator().next())),
                () -> assertEquals(workspace.getCreatedBy(), getValue(mvcResult, "$.createdBy")),
                () -> assertEquals(String.valueOf(LocalDate.now()), getValue(mvcResult, "$.createdDate")),
                () -> assertEquals(workspace.getUpdatedBy(), getValue(mvcResult, "$.updatedBy")),
                () -> assertEquals(String.valueOf(LocalDate.now()), getValue(mvcResult, "$.updatedDate")),
                () -> assertEquals(1, members.size())
        );
    }

    @Test
    public void updateFailure() throws Exception {
        Workspace workspace = creator.getNewWorkspace("o@gmail.com");
        workspace.setName(null);
        workspace.setUpdatedBy(workspace.getCreatedBy());

        MvcResult firstMvcResult = super.update(URL, workspace.getId(), workspace);
        assertEquals(HttpStatus.BAD_REQUEST.value(), firstMvcResult.getResponse().getStatus());
    }

    @Test
    public void deleteById() throws Exception {
        Workspace workspace = creator.getNewWorkspace("s@gmail.com");
        MvcResult mvcResult = super.delete(URL, workspace.getId());
        MvcResult result = super.readAll(URL);
        List<Workspace> testWorkspaces = getResourceArray(result);

        assertAll(
                () -> assertEquals(HttpStatus.OK.value(), mvcResult.getResponse().getStatus()),
                () -> assertFalse(testWorkspaces.contains(workspace))
        );
    }

    @Test
    public void deleteByIdFailure() throws Exception {
        MvcResult mvcResult = super.delete(URL, UUID.randomUUID());
        assertEquals(HttpStatus.BAD_REQUEST.value(), mvcResult.getResponse().getStatus());
    }

    @Test
    public void findAll() throws Exception {
        Workspace firstWorkspace = creator.getNewWorkspace("v@gmail.com");
        Workspace secondWorkspace = creator.getNewWorkspace("r@gmail.com");
        MvcResult mvcResult = super.readAll(URL);
        List<Workspace> testWorkspaces = getResourceArray(mvcResult);

        assertAll(
                () -> assertEquals(MediaType.APPLICATION_JSON.toString(), mvcResult.getResponse().getContentType()),
                () -> assertEquals(HttpStatus.OK.value(), mvcResult.getResponse().getStatus()),
                () -> assertTrue(testWorkspaces.contains(firstWorkspace)),
                () -> assertTrue(testWorkspaces.contains(secondWorkspace))
        );
    }

    @Test
    public void findById() throws Exception {
        Workspace workspace = creator.getNewWorkspace("t@gmail.com");
        MvcResult result = super.readById(URL, workspace.getId());
        Set<UUID> testMembers = creator.getIdsFromJson(getValue(result, "$.members").toString());

        assertAll(
                () -> assertEquals(HttpStatus.OK.value(), result.getResponse().getStatus()),
                () -> assertNotNull(getValue(result, "$.id")),
                () -> assertEquals(workspace.getName(), getValue(result, "$.name")),
                () -> assertEquals(workspace.getDescription(), getValue(result, "$.description")),
                () -> assertEquals(WorkspaceVisibility.PUBLIC.toString(), getValue(result, "$.visibility")),
                () -> assertTrue(testMembers.contains(workspace.getMembers().iterator().next())),
                () -> assertEquals(workspace.getCreatedBy(), getValue(result, "$.createdBy")),
                () -> assertEquals(String.valueOf(LocalDate.now()), getValue(result, "$.createdDate")),
                () -> assertNull(getValue(result, "$.updatedBy")),
                () -> assertNull(getValue(result, "$.updatedDate")),
                () -> assertEquals(1, testMembers.size())
        );
    }

    @Test
    public void findByIdFailure() throws Exception {
        MvcResult mvcResult = super.readById(URL, UUID.randomUUID());
        assertEquals(HttpStatus.NOT_FOUND.value(), mvcResult.getResponse().getStatus());
    }

}
