package spd.trello;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.jayway.jsonpath.JsonPath;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import spd.trello.domain.common.Domain;
import spd.trello.domain.common.Resource;


import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;


import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.UUID;


@AutoConfigureMockMvc
public class AbstractIntegrationTest<E extends Resource> {

    @Autowired
    private MockMvc mockMvc;

    private final ObjectMapper objectMapper = new ObjectMapper().findAndRegisterModules();

    public MvcResult create(String url, E entity) throws Exception {
        return mockMvc.perform(post(url, entity)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(entity)))
                .andReturn();
    }

    public MvcResult update(String url, UUID id, E entity) throws Exception {
        return mockMvc.perform(MockMvcRequestBuilders.put(url + "/{id}", id, entity)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(entity)))
                .andReturn();
    }

    public MvcResult delete(String url, UUID id) throws Exception {
        return mockMvc.perform(MockMvcRequestBuilders.delete(url + "/{id}", id)
                        .accept(MediaType.APPLICATION_JSON))
                .andReturn();
    }

    public MvcResult readById(String url, UUID id) throws Exception {
        return mockMvc.perform(MockMvcRequestBuilders.get(url + "/{id}",id)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andReturn();
    }

    public MvcResult readAll(String url) throws Exception {
        return mockMvc.perform(get(url))
                .andReturn();
    }

    public Object getValue(MvcResult mvcResult, String jsonPath) throws UnsupportedEncodingException {
        return JsonPath.read(mvcResult.getResponse().getContentAsString(), jsonPath);
    }

    public List<E> getResourceArray(MvcResult mvcResult) throws UnsupportedEncodingException, JsonProcessingException {
        return new ObjectMapper().findAndRegisterModules().readValue(mvcResult.getResponse().getContentAsString(), new TypeReference<>() {});
    }
}
