package spd.trello.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import lombok.EqualsAndHashCode;
import spd.trello.domain.common.Domain;

import javax.persistence.*;
import java.util.UUID;
@Entity
@Table
@Data
@EqualsAndHashCode(callSuper = true)
public class CheckAbleItem extends Domain {

    private String name;

    private Boolean checked;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "check_list_id", referencedColumnName = "id")
    @JsonIgnore
    private CheckList checkList;
}