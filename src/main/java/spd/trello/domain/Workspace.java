package spd.trello.domain;

import lombok.Data;

import java.util.*;

import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import spd.trello.domain.common.Resource;
import spd.trello.domain.type.WorkspaceVisibility;

import javax.persistence.*;

@Entity
@Table
@Data
@EqualsAndHashCode(callSuper = true)
public class Workspace extends Resource {

    private String name;

    private String description;

    @Enumerated(EnumType.STRING)
    private WorkspaceVisibility visibility= WorkspaceVisibility.PRIVATE;

    @ElementCollection
    @LazyCollection(LazyCollectionOption.FALSE)
    @CollectionTable(
            name = "workspaces_members",
            joinColumns=@JoinColumn(name= "workspace_id")
    )
    @Column(name = "member_id")
    private Set<UUID> members = new HashSet<>();

    @ElementCollection
    @LazyCollection(LazyCollectionOption.FALSE)
    @CollectionTable(
            name = "board",
            joinColumns=@JoinColumn(name= "workspace_id")
    )
    @Column(name = "id")
    @Cascade(org.hibernate.annotations.CascadeType.ALL)
    private Set<UUID> boards = new HashSet<>();
}
