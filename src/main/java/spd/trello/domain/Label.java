package spd.trello.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import spd.trello.domain.common.Domain;

import javax.persistence.*;
import java.util.*;

@Entity
@Table
@Data
@EqualsAndHashCode(callSuper = true)
public class Label extends Domain {

    private String name;

    @ManyToOne
    @JoinColumn(name = "color_id")
    private Color color;

    @ElementCollection
    @LazyCollection(LazyCollectionOption.FALSE)
    @CollectionTable(
            name = "cards_labels",
            joinColumns=@JoinColumn(name= "label_id")
    )
    @Column(name = "card_id")
    private Set<UUID> cards = new HashSet<>();
}
