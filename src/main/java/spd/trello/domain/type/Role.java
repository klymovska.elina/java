package spd.trello.domain.type;

public enum Role {
    GUEST, MEMBER, ADMIN
}