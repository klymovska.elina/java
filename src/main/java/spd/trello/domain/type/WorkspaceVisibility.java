package spd.trello.domain.type;

public enum WorkspaceVisibility {
    PRIVATE, PUBLIC
}
