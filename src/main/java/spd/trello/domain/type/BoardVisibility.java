package spd.trello.domain.type;

public enum BoardVisibility {
    PRIVATE, WORKSPACE, PUBLIC
}
