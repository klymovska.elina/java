package spd.trello.domain.type;

import lombok.Data;

import lombok.EqualsAndHashCode;
import spd.trello.domain.common.Domain;
import spd.trello.domain.common.Resource;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table
@Data
@EqualsAndHashCode(callSuper = true)
public class CardTemplate extends Domain {
}
