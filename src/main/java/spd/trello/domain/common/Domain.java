package spd.trello.domain.common;

import lombok.Data;

import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.util.UUID;
@MappedSuperclass
@Data
public abstract class Domain {
    @Id
    private UUID id = UUID.randomUUID();
}
