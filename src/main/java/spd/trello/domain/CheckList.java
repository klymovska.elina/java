package spd.trello.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import java.util.*;

import lombok.EqualsAndHashCode;
import spd.trello.domain.common.Resource;

import javax.persistence.*;

@Entity
@Table(name = "checklist")
@Data
@EqualsAndHashCode(callSuper = true)
public class CheckList extends Resource {

    private String name;

    @OneToMany(mappedBy = "checkList", cascade = CascadeType.ALL)
    @JsonIgnore
    private List<CheckAbleItem> items = new ArrayList<>();

    @Column(name = "card_id")
    private UUID cardId;
}
