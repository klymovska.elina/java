package spd.trello.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;
import spd.trello.domain.common.Resource;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.File;
import java.util.UUID;

@Entity
@Table(name = "attachment")
@Data
@EqualsAndHashCode(callSuper = true)
public class Attachment extends Resource {
    @Column(name = "name")
    private String name;
    @Column(name = "file")
    private String file;
    @Column(name = "comment_id")
    private UUID commentId;
    @Column(name = "card_id")
    private UUID cardId;
}
