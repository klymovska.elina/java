package spd.trello.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import lombok.EqualsAndHashCode;
import spd.trello.domain.common.Domain;
import spd.trello.domain.common.Resource;

import javax.persistence.*;

@Entity
@Table
@Data
@EqualsAndHashCode(callSuper = true)
public class Reminder extends Domain {
    private LocalDateTime startAt;
    private LocalDateTime endAt;
    private LocalDateTime remindOn;
    private Boolean active;

    @OneToOne (mappedBy = "reminder", cascade = CascadeType.ALL)
    @JsonIgnore
    private Card card;
}
