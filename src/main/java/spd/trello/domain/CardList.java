package spd.trello.domain;

import lombok.Data;

import java.util.*;

import lombok.EqualsAndHashCode;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import spd.trello.domain.common.Domain;
import spd.trello.domain.common.Resource;

import javax.persistence.*;

@Entity
@Table(name = "cardlist")
@Data
@EqualsAndHashCode(callSuper = true)
public class CardList extends Resource {

    private String name;

    @ElementCollection
    @LazyCollection(LazyCollectionOption.FALSE)
    @CollectionTable(
            name = "card",
            joinColumns=@JoinColumn(name= "cardlist_id")
    )
    @Column(name = "id")
    private Set<UUID> cards = new HashSet<>();

    private Boolean archived;

    @Column(name = "board_id")
    private UUID boardId;
}
