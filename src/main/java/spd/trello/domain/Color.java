package spd.trello.domain;
import lombok.Data;
import lombok.EqualsAndHashCode;
import spd.trello.domain.Label;
import spd.trello.domain.common.Domain;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.List;

@Entity
@Table
@Data
@EqualsAndHashCode(callSuper = true)
public class Color extends Domain {
    private String color;
    @OneToMany(mappedBy = "color")
    private List<Label> labels;
}
