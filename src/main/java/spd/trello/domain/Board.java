package spd.trello.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import spd.trello.domain.common.Resource;
import spd.trello.domain.type.BoardVisibility;

import javax.persistence.*;
import java.util.*;

@Entity
@Table
@Data
@EqualsAndHashCode(callSuper = true)
public class Board extends Resource {
    private String name;
    private String description;
    @Column(name = "workspace_id")
    private UUID workspaceId;
    @Enumerated(EnumType.STRING)
    private BoardVisibility visibility = BoardVisibility.PUBLIC;
    private Boolean archived = false;

    @ElementCollection
    @LazyCollection(LazyCollectionOption.FALSE)
    @CollectionTable(
            name = "cardlist",
            joinColumns=@JoinColumn(name= "board_id")
    )
    @Column(name = "id")
    @Cascade(org.hibernate.annotations.CascadeType.ALL)
    private Set<UUID> cardLists = new HashSet<>();

    @ElementCollection
    @LazyCollection(LazyCollectionOption.FALSE)
    @CollectionTable(
            name = "boards_members",
            joinColumns=@JoinColumn(name= "board_id")
    )
    @Column(name = "member_id")
    private Set<UUID> members = new HashSet<>();
}
