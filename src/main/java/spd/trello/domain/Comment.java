package spd.trello.domain;

import lombok.Data;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import lombok.EqualsAndHashCode;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import spd.trello.domain.common.Resource;

import javax.persistence.*;

@Entity
@Table
@Data
@EqualsAndHashCode(callSuper = true)
public class Comment extends Resource {

    @Column(name = "member_id")
    private UUID memberId;

    private String text;

    private LocalDateTime localDateTime;

    @ElementCollection
    @LazyCollection(LazyCollectionOption.FALSE)
    @CollectionTable(
            name = "attachment",
            joinColumns=@JoinColumn(name= "comment_id")
    )
    @Column(name = "id")
    private Set<UUID> attachments = new HashSet<>();

    @Column(name = "card_id")
    private UUID cardId;
}
