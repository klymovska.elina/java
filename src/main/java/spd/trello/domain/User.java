package spd.trello.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.lang.NonNull;
import spd.trello.domain.common.Resource;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;


@Getter
@Setter
@ToString(callSuper = true)
@Entity
@Table(name = "users")
public class User extends Resource {
    @NotEmpty
    @NonNull
    @Size(min = 2, max = 40)
    @Column(name = "first_name")
    private String firstName;
    @NotEmpty
    @NonNull
    @Size(min = 2, max = 40)
    @Column(name = "last_name")
    private String lastName;
    @NotEmpty
    @NonNull
    @Size(min = 2, max = 40)
    @Column(name = "email")
    private String email;
/*
    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
    private List<Member> members = new ArrayList<>();*/

}