package spd.trello.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.springframework.lang.NonNull;
import spd.trello.domain.common.Resource;
import spd.trello.domain.type.Role;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.util.*;

@Entity
@Getter
@Setter
@Table(name = "member")
public class Member extends Resource {

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    private User user;

    @Column(name = "role")
    @Enumerated(EnumType.STRING)
    @NotEmpty
    @NonNull
    private Role role;

    @ElementCollection
    @LazyCollection(LazyCollectionOption.FALSE)
    @CollectionTable(
            name = "workspaces_members",
            joinColumns=@JoinColumn(name= "member_id")
    )
    @Column(name = "workspace_id")
    private Set<UUID> workspaces = new HashSet<>();

    @ElementCollection
    @LazyCollection(LazyCollectionOption.FALSE)
    @CollectionTable(
            name = "boards_members",
            joinColumns=@JoinColumn(name= "member_id")
    )
    @Column(name = "board_id")
    private Set<UUID> boards = new HashSet<>();


    @ElementCollection
    @LazyCollection(LazyCollectionOption.FALSE)
    @CollectionTable(
            name = "cards_members",
            joinColumns=@JoinColumn(name= "member_id")
    )
    @Column(name = "card_id")
    private  Set<UUID> cards = new HashSet<>();

    @ElementCollection
    @LazyCollection(LazyCollectionOption.FALSE)
    @CollectionTable(
            name = "comment",
            joinColumns=@JoinColumn(name= "member_id")
    )
    @Column(name = "id")
    private Set<UUID> comments = new HashSet<>();
}
