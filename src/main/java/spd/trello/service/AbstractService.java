package spd.trello.service;


import spd.trello.domain.common.Resource;
import spd.trello.exception.ResourceNotFoundException;
import spd.trello.repository.AbstractRepository;

import java.util.List;
import java.util.UUID;

public abstract class AbstractService<E extends Resource, R extends AbstractRepository<E>>
        implements CommonService<E>{
    R repository;

    public AbstractService(R repository){
        this.repository = repository;
    }

    @Override
    public E create(E entity) {
/*        entity.setCreatedDate(entity.getCreatedDate().minusNanos(entity.getCreatedDate().getNano()));
        entity.setUpdatedDate(entity.getCreatedDate());
        System.out.println("Service *******");
        System.out.println(entity.getCreatedDate());
        System.out.println(entity.getUpdatedDate());*/
        return repository.save(entity);
    }

    @Override
    public E update(E entity) {
        entity.setUpdatedDate(entity.getUpdatedDate().minusNanos(entity.getUpdatedDate().getNano()));
        return repository.save(entity);
    }


    @Override
    public E readById (UUID id) {
        return repository.findById(id).orElseThrow(
                () -> new ResourceNotFoundException("Entity not found"));    }

    @Override
    public List<E> getAll() {
        return repository.findAll();
    }
    @Override
    public void delete(UUID id) {
        repository.deleteById(id);
    }
}
