package spd.trello.repository;

import org.springframework.stereotype.Repository;
import spd.trello.domain.CardList;
@Repository
public interface CardListRepository extends AbstractRepository<CardList> {
}
