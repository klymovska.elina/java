package spd.trello.repository;

import org.springframework.stereotype.Repository;
import spd.trello.domain.Member;

import java.util.List;

@Repository
public interface MemberRepository extends AbstractRepository<Member> {
}
