package spd.trello.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import spd.trello.domain.Attachment;
import spd.trello.service.AttachmentService;

@RestController
@RequestMapping("/attachments")
public class AttachmentController extends AbstractController<Attachment, AttachmentService> {
    @Autowired
    public AttachmentController(AttachmentService service) {
        super(service);
    }
}
