CREATE TABLE check_able_item
(
    id              UUID PRIMARY KEY,
    name            VARCHAR(50) NOT NULL,
    checked boolean      NOT NULL,
    cardlist_id    UUID REFERENCES cardlist (id)
);