CREATE TABLE color
(
    id      UUID PRIMARY KEY,
    color varchar(50)
);