CREATE TABLE workspaces_members
(
    workspace_id UUID NOT NULL REFERENCES workspace(id),
    member_id  UUID NOT NULL REFERENCES member(id),
    CONSTRAINT workspaces_members_pk PRIMARY KEY (workspace_id, member_id)
);
