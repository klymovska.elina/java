CREATE TABLE comment
(
    id            UUID PRIMARY KEY,
    text VARCHAR (225) NOT NULL,
    member_id     UUID REFERENCES member (id),
    localDateTime timestamp without time zone NOT NULL,

    card_id    UUID REFERENCES card (id),

    created_by varchar(50) NOT NULL,
    updated_by varchar(50),
    created_date timestamp NOT NULL,
    updated_date timestamp
);