CREATE TABLE label
(
    id       UUID PRIMARY KEY,
    name     VARCHAR(50) NOT NULL,
    color_id UUID REFERENCES color (id)
);