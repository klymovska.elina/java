CREATE TABLE boardtemplate
(
    id      UUID PRIMARY KEY,
    board_id    UUID REFERENCES board (id)
);

CREATE TABLE cardtemplate
(
    id      UUID PRIMARY KEY,
    card_id UUID NOT NULL REFERENCES card (id)
);
