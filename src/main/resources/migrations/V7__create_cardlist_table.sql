CREATE TABLE cardlist
(
    id          UUID PRIMARY KEY,
    name        VARCHAR(50) NOT NULL,
    archived    boolean DEFAULT false,
    board_id    UUID REFERENCES board (id),

    created_by varchar(50) NOT NULL,
    updated_by varchar(50),
    created_date timestamp NOT NULL,
    updated_date timestamp
);
