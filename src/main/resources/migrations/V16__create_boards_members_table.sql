CREATE TABLE boards_members
(
    board_id UUID NOT NULL REFERENCES board(id),
    member_id  UUID NOT NULL REFERENCES member(id),
    CONSTRAINT boards_members_pk PRIMARY KEY (board_id, member_id)
);
