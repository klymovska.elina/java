CREATE TABLE cards_labels
(
    card_id UUID NOT NULL REFERENCES card(id),
    label_id  UUID NOT NULL REFERENCES label(id),
    CONSTRAINT cards_labels_pk PRIMARY KEY (card_id, label_id)
);
