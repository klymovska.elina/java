CREATE TABLE member
(
    id      UUID PRIMARY KEY,
    user_id UUID NOT NULL REFERENCES users (id),
    role varchar(10) NOT NULL,
    created_by varchar(50) NOT NULL,
    updated_by varchar(50),
    created_date timestamp NOT NULL,
    updated_date timestamp
);