CREATE TABLE card
(
    id          UUID PRIMARY KEY,
    name        VARCHAR(50) NOT NULL,
    description VARCHAR(225) NOT NULL,
    archived    boolean      NOT NULL,
    reminder_id UUID REFERENCES reminder (id),
    cardlist_id UUID REFERENCES cardlist (id),

    created_by varchar(50) NOT NULL,
    updated_by varchar(50),
    created_date timestamp NOT NULL,
    updated_date timestamp
);