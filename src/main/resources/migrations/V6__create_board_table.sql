CREATE TABLE board
(
    id           UUID PRIMARY KEY,
    name         VARCHAR(50) NOT NULL,
    description  VARCHAR(225) NOT NULL,
    workspace_id UUID REFERENCES workspace (id),
    visibility   VARCHAR(20) DEFAULT 'PUBLIC',
    archived     boolean         DEFAULT false,

    created_by varchar(50) NOT NULL,
    updated_by varchar(50),
    created_date timestamp NOT NULL,
    updated_date timestamp
);