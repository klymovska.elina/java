CREATE TABLE cards_members
(
    card_id UUID NOT NULL REFERENCES card(id),
    member_id  UUID NOT NULL REFERENCES member(id),
    CONSTRAINT cards_members_pk PRIMARY KEY (card_id, member_id)
);
