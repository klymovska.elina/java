CREATE TABLE workspace
(
    id          UUID PRIMARY KEY,
    name        VARCHAR(50)                NOT NULL,
    description VARCHAR(225)                NOT NULL,
    visibility  VARCHAR(20) DEFAULT 'PUBLIC',

    created_by varchar(50) NOT NULL,
    updated_by varchar(50),
    created_date timestamp NOT NULL,
    updated_date timestamp
);
