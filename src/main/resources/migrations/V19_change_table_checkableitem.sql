ALTER TABLE check_able_item DROP COLUMN cardlist_id;
ALTER TABLE check_able_item ADD COLUMN checklist_id UUID REFERENCES checklist (id);
