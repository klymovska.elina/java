CREATE TABLE checklist
(
    id          UUID PRIMARY KEY,
    name        VARCHAR(50) NOT NULL,

    card_id    UUID REFERENCES card (id),

    created_by varchar(50) NOT NULL,
    updated_by varchar(50),
    created_date timestamp NOT NULL,
    updated_date timestamp
);