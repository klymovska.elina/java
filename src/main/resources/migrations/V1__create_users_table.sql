CREATE TABLE users
(
    id        UUID PRIMARY KEY,
    first_name varchar(50) NOT NULL,
    last_name  varchar(50) NOT NULL,
    email     varchar(50) NOT NULL,
    created_by varchar(50) NOT NULL,
    updated_by varchar(50),
    created_date timestamp NOT NULL,
    updated_date timestamp
);
