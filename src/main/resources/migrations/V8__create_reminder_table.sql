CREATE TABLE reminder
(
    id          UUID PRIMARY KEY,
    start_at  timestamp without time zone NOT NULL,
    end_at    timestamp without time zone NOT NULL,
    remind_on    timestamp without time zone NOT NULL,
    active     boolean DEFAULT false
);